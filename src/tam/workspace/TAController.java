package tam.workspace;

import djf.ui.AppGUI;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import java.util.ArrayList;

import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    AppGUI gui;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        gui = app.getGUI();
        
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty() || email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            if(name.isEmpty())
            {
                dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));     
            }
            if(email.isEmpty())
            {
                dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));    
            }
	           
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            data.addTA(name, email);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            emailTextField.requestFocus();
        }
        
        //Handles save button enable
        gui.updateToolbarControls(false);
        
        
    }

    public void deleteTA(){
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        if(selectedItem != null)
        {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName = ta.getName();
            
            TAData data = (TAData)app.getDataComponent();
            //remove ta from the list
            data.getTeachingAssistants().remove(ta);
            //Get keys from each TA object and delete each item that has the key
            ArrayList<String> list = ta.getKeys();
            for(int i = list.size()-1; i >= 0 ; i--)
            {
                String[] key = list.get(i).split("_");
                data.removeTAFromCell(data.getCellTextProperty(Integer.parseInt(key[0]), Integer.parseInt(key[1])), taName);
                ta.removeKey(list.get(i));
            }
        }
        //Handles save button enable
        gui.updateToolbarControls(false);
    }
    
    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        //If nothing is selected
        if(selectedItem != null)
        {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            
            String taName = ta.getName();
            
            TAData data = (TAData)app.getDataComponent();
            
            String key = workspace.getCellKey(pane);
            //Get the key
            String[] split = key.split("_");
            int col = Integer.parseInt(split[0]);
            int row = Integer.parseInt(split[1]);
            //If the data contains the TA, toggle the cell
            if(data.getCellTextProperty(col, row).getValue().contains(taName))
            {
                data.removeTAFromCell(data.getCellTextProperty(col, row), taName);
                ta.removeKey(key);
            }
            else
            {
                String cellKey = pane.getId();
                ta.addKey(key);
                // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
                data.toggleTAOfficeHours(cellKey, taName);
            }
            
        }
        //Handles save button enable
        gui.updateToolbarControls(false);
    }
    
}